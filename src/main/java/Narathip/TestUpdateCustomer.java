/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Narathip;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class TestUpdateCustomer {
  
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE Customer SET FirstName = ?, LastName = ?,Tel = ? WHERE Customer_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "aaa");
            stmt.setString(2, "bbb");
            stmt.setInt(3, 4);
            stmt.setInt(4, 10);
            int row =stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();

    }  
}
