///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.customer.poc;
//
//import database.Database;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import model.Customer;
//
///**
// *
// * @author werapan
// */
//public class TestSelectCustomer {
//
//    public static void main(String[] args) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        try {
//            String sql = "SELECT Customer_ID, FirstName, LastName, Tel FROM Customer";
//            Statement stmt = conn.createStatement();
//            ResultSet result = stmt.executeQuery(sql);
//            while(result.next()) {
//                int Customer_ID = result.getInt("Customer_ID");
//                String FirstName = result.getString("FirstName");
//                String LastName = result.getString("LastName");
//                int Tel = result.getInt("Tel");
//                Customer customer = new Customer(Customer_ID, FirstName, LastName,Tel);
//                System.out.println(customer);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        db.close();
//    }
//}
