/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Watchara;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Watchara.User;

/**
 *
 * @author ACER
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;       
        try {
            String sql = "INSERT INTO User (username,password) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPassword());           
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);             
                object.setId(id);             
            }
        }catch (SQLException ex) {
            System.out.println("Error: add " + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT User_ID, Username, Password, Type FROM User ";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("User_ID");
                String username = result.getString("Username");
                String password = result.getString("Password");  
                String type = result.getString("Type"); 
                User user = new User(id, username, password, type);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT User_ID, Username, Password, Type FROM User ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int gid = result.getInt("User_ID");
                String username = result.getString("Username");
                String password = result.getString("Password");  
                String type = result.getString("Type"); 
                User user = new User(gid, username, password, type);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get " + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM User WHERE User_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id " + id + "!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE User SET Username = ?, Password = ?, Type = ? WHERE User_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getType());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: update " + ex.getMessage());
        }
        db.close();
        return row;
    }

    public ArrayList<User> getData(String keyword) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM User WHERE Username LIKE"+"\'"+keyword+"%"+"\'"+"OR Type Like"+"\'"+keyword+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("User_ID");
                String name = result.getString("Username");
                String password = result.getString("Password"); 
                String type = result.getString("Type"); 
                User employee = new User(id, name, password , type);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
    
    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
    }
    
}
