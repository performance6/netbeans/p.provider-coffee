/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Watchara;

import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class UserService {
    private static ArrayList<User> userList;
    private static int idLogin;
    private static int typeLogin;
    
    private static User userLogin;

    public static ArrayList<User> getList() {
        return userList;
    }

    public static void load() {
        UserDao dao = new UserDao();
        userList = dao.getAll();
    }

    private static void userauth(User user,int id) {
        userLogin=user;
        idLogin = id;
    }

    public static boolean auth(String username, String password) {
        for (User user : userList) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                userauth(user,user.getId());
                return true;
            }
        }
        return false;
    }

    public static int getIdLogin() {
        return idLogin;
    }

    public static int getTypeLogin() {
        return typeLogin;
    }

    public static User getUserLogin() {
        return userLogin;
    } 
}
