/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sarunporn;

/**
 *
 * @author Dell
 */
public class EmployeeMain extends javax.swing.JDialog{
    
    public EmployeeMain(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
//        initComponents();
        EmployeePanel panel = new EmployeePanel();
        this.add(panel);
        this.pack();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TestEmployeePanel dialog = new TestEmployeePanel(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
}
